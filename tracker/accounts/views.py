from contextlib import redirect_stderr
from django.shortcuts import render
from django.forms import ModelForm
from myapp.models import Article
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

# Create the form class.


class ArticleForm(ModelForm):
    class Meta:
        model = Article


fields = ['pub_date', 'headline', 'content', 'reporter']


form = ArticleForm()


article = Article.objects.get(pk=1)
form = ArticleForm(instance=article)


user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
user.last_name = 'Lennon'
user.save()


def my_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        redirect_stderr("home")
    else:
        # Return an 'invalid login' error message.
        return "invalid login"