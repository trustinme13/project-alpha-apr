from os import path
from xml.etree.ElementInclude import include

path("", include("list_projects.urls"))

path("projects/", include("projects.urls"))