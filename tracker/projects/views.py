from contextlib import redirect_stderr
from msilib.schema import ListView
from django.shortcuts import render

from tracker.projects.models import Project

# Create your views here.


class ProjectListView(ListView):
    model = Project
    template_name = "projects/list.html"


def show_Project(request):

    model_list = Project.objects.all()
    
    context = {
        "model_list": model_list
    }

    return redirect_stderr(request, "projects/list.html", context)
